import dotenv from 'dotenv'
dotenv.config()
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	app: {
		head: {
			titleTemplate: 'Samuel ORTION - %s',
			htmlAttrs: {
				lang: 'en'
			},
			meta: [
				{ charset: 'utf-8' },
				{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
				{ hid: 'description', name: 'description', content: 'The Web Home of Samuel ORTION' },
				{ name: 'format-detection', content: 'telephone=no' }
			],
			link: [
				{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
			]
		},
	},
	modules: [
		'@nuxtjs/i18n',
		'@nuxtjs/color-mode',
		'@nuxt/content',
	],
	plugins: [
		'~/plugins/fontawesome.js'
	],
	css: [
		'~/assets/css/main.css',
		'@fortawesome/fontawesome-svg-core/styles.css'
	],
	nitro: {
		prerender: {
			failOnError: false,
		}
	},
	postcss: {
		plugins: {
			tailwindcss: {},
			autoprefixer: {},
		},
	},
	i18n: {
		locales: [
			{
				code: 'en',
				name: 'English',
				iso: 'en-US',
				file: 'en.json'
			},
			{
				code: 'fr',
				name: 'Français',
				iso: 'fr-FR',
				file: 'fr.json'
			}
		],
		defaultLocale: 'fr',
		lazy: true,
		langDir: 'lang/'
	},
	// mail: {
	// 	message: {
	// 		to: process.env.MAIL_TO
	// 	},
	// 	smtp: {
	// 		host: process.env.MAIL_SMTP_HOST,
	// 		port: process.env.MAIL_SMTP_PORT,
	// 		auth: {
	// 			user: process.env.MAIL_SMTP_USER,
	// 			pass: process.env.MAIL_SMTP_PASS
	// 		}
	// 	}
	// },
})
