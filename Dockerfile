FROM node:18.14.0

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

COPY . ./

RUN yarn install

RUN yarn build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "node", ".output/server/index.mjs" ]
