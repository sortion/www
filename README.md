# [samuel.ortion.fr](https://samuel.ortion.fr/)

My home web page, built with Nuxt.

## Run

```{bash}
yarn dev
```

## Build static

```{bash}
yarn generate
```
